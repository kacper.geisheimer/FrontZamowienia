# Wymagania

* npm 6.10.0
* node v12.13.0

# Konfiguracja

Nazwę pliku`.env.dist` należy na `.env` i wypełnić danymi w celu poprawnego działania aplikacji

# Uruchomienie

1. Przed pierwszym uruchomieniem serwera developerskiego należy wykonać `npm install`
2. Uruchomienie serwera developerskiego `npm start`
