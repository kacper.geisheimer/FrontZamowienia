import { RestaurantType, MealRestaurantType } from "./restaurantsType";
import { BasketType, PaymentMethodType } from "./basketTypes";
import { OrdersType, SingleOrderType, RewardType, UserRewardType } from "./profileType";

export interface RootState {
  readonly auth: AuthState;
  readonly restaurants: RestaurantsState;
  readonly basket: BasketState;
  readonly profile: ProfileState;
};

export interface AuthState {
  readonly user: User | null | undefined;
  readonly startLogin: boolean;
  readonly startRegister: boolean;
};

export interface BasketState {
  readonly basket: BasketType[];
  readonly isAddingOrder: boolean;
  readonly isAddingToBasket: boolean;
  readonly isUpdateingBasket: boolean;
  readonly paymentMethods: PaymentMethodType[];
  readonly isFetchingPaymentMethods: boolean;
  readonly selectedPaymentMethod: number | null;
  readonly creatingOrder: boolean;
  readonly newOrderId: string;
};

export interface ProfileState {
  readonly orders: OrdersType[];
  readonly isFetchingOrders: boolean;
  readonly singleOrder: SingleOrderType | null;
  readonly isFetchingSingleOrder: boolean;
  readonly rewards: RewardType[] | null;
  readonly isFetchingRewards: boolean;
  readonly userRewards: UserRewardType[] | null;
  readonly isFetchingUserReward: boolean;
  readonly isAddingReward: boolean;
};

export interface RestaurantsState {
  readonly restaurants: RestaurantType[];
  readonly isFetchingRestaurants: boolean;
  readonly isFetchedRestaurants: boolean;
  readonly restaurant: RestaurantType;
  readonly isFetchingRestaurant: boolean;
  readonly isFetchedRestaurant: boolean;
  readonly isFetchingMealsRestaurant: boolean;
  readonly restaurantMeals: MealRestaurantType[] | [];
};

export interface MyAction {
  type: string;
  payload?: any;
}

export interface User {
  readonly id: number;
  readonly email: string;
  readonly first_name: string;
  readonly last_name: string;
  readonly points: string;
  readonly groups: string[];
}