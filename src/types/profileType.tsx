export interface OrdersType {
    id: string;
    user: number;
    created: any;
    status: string;
}

export interface SingleOrderType {
    status_payment: string;
    payment_method: string;
    meals: SingleOrderMealsType[];
}

export interface SingleOrderMealsType {
    count: number;
    meal: string;
    price: number;
}

export interface RewardType {
    id: number;
    name: string;
    cost: number;
}

export interface UserRewardType {
    status: string;
    reward: string;
}