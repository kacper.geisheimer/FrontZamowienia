export interface RestaurantType {
    id: number;
    name: string;
    street: string;
    number: string;
    number_local: string;
    zip_code: string;
    city: string;
    photo: string;
    owner: number;
    detail?: any;
}

export interface MealRestaurantType {
    id: number;
    name: string;
    price: number;
    restaurant: number;
}