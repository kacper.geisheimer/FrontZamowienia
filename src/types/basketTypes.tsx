export interface BasketType {
    id: number;
    name: string;
    price: number;
    count: number;
}

export interface PaymentMethodType {
    id: number;
    name: string;
}