export interface Routes {
    homeView: string;
    loginView: string;
    registerView: string;
    restaurantView: string;
    profileView: string;
    basketView: string;
    orderView: string;
}