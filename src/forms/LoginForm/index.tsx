import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { Button } from '../../components'

export interface LoginFormProps {
  login(email: string, password: string): void;
  startLogin: boolean;
}

const LoginForm = (props: LoginFormProps) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleKeyDown = (e: any) => {
    if (e.key === 'Enter') {
      login();
      setPassword('');
    }
  }

  const login = () => {
    props.login(email, password);
  }

  const handleSubmit = (e: any) => {
    e.preventDefault();

    login();
  }

  return (
    <Form>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="Email..."
          value={email}
          onChange={e => setEmail(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </Form.Group>
      <Form.Group controlId="formBasicPassword">
        <Form.Label>Hasło</Form.Label>
        <Form.Control
          type="password"
          placeholder="Hasło..."
          value={password}
          onChange={e => setPassword(e.target.value)}
          onKeyDown={handleKeyDown}
        />
      </Form.Group>
      <Button 
        loading={props.startLogin}
        onClick={handleSubmit}
      >
        Zaloguj się
      </Button>
    </Form>
  );
}

export default LoginForm;