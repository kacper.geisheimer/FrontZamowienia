import React, { useState } from 'react';
import { Form } from 'react-bootstrap';
import { Button } from '../../components'
import { Register } from '../../types';

export interface RegisterFormProps {
  register(data: Register): void;
  startRegister: boolean;
}

const RegisterForm = (props: RegisterFormProps) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');


  const handleSubmit = (e: any) => {
    e.preventDefault();

    props.register({
      email,
      password
    });
  }

  return (
    <Form>
      <Form.Group controlId="formBasicEmail">
        <Form.Label>Email</Form.Label>
        <Form.Control
          type="email"
          placeholder="Email..."
          value={email}
          onChange={e => setEmail(e.target.value)}
        />
      </Form.Group>
      <Form.Group controlId="formBasicPassword">
        <Form.Label>Hasło</Form.Label>
        <Form.Control
          type="password"
          placeholder="Hasło..."
          value={password}
          onChange={e => setPassword(e.target.value)}
        />
      </Form.Group>
      <Button 
        onClick={handleSubmit}
        loading={props.startRegister}
      >
        Zarejestruj się
      </Button>
    </Form>
  );
}

export default RegisterForm;