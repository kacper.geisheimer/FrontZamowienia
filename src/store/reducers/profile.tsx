import { MyAction, ProfileState } from '../../types';
import {
  FETCH_ORDERS,
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_FAILED,
  FETCH_SINGLE_ORDER,
  FETCH_SINGLE_ORDER_SUCCESS,
  FETCH_SINGLE_ORDER_FAILED,
  FETCH_REWARDS,
  FETCH_REWARDS_SUCCESS,
  FETCH_REWARDS_FAILED,
  FETCH_SINGLE_REWARD,
  FETCH_SINGLE_REWARD_SUCCESS,
  FETCH_SINGLE_REWARD_FAILED,
  BUY_REWARD,
  BUY_REWARD_SUCCESS,
  BUY_REWARD_FAILED
} from '../constants/profile';

const initialState: ProfileState = {
  orders: [],
  isFetchingOrders: false,
  singleOrder: null,
  isFetchingSingleOrder: null,
  rewards: [],
  isFetchingRewards: false,
  userRewards: null,
  isFetchingUserReward: false,
  isAddingReward: false,
};

const profile = (state = initialState, action: MyAction) => {
  switch (action.type) {
    case FETCH_ORDERS:
      return {
        ...state,
        isFetchingOrders: true,
      }
    case FETCH_ORDERS_SUCCESS:
      return {
        ...state,
        orders: action.payload,
        isFetchingOrders: false,
      }
    case FETCH_ORDERS_FAILED:
      return {
        ...state,
        orders: [],
        isFetchingOrders: true,
      }
    case FETCH_SINGLE_ORDER:
      return {
        ...state,
        isFetchingSingleOrder: true,
      }
    case FETCH_SINGLE_ORDER_SUCCESS:
      return {
        ...state,
        singleOrder: action.payload,
        isFetchingSingleOrder: false,
      }
    case FETCH_SINGLE_ORDER_FAILED:
      return {
        ...state,
        singleOrder: null,
        isFetchingSingleOrder: false,
      }
    case FETCH_REWARDS:
      return {
        ...state,
        isFetchingRewards: true,
      }
    case FETCH_REWARDS_SUCCESS:
      return {
        ...state,
        rewards: action.payload,
        isFetchingRewards: false,
      }
    case FETCH_REWARDS_FAILED:
      return {
        ...state,
        rewards: null,
        isFetchingRewards: false,
      }
    case FETCH_SINGLE_REWARD:
      return {
        ...state,
        isFetchingUserReward: true,
      }
    case FETCH_SINGLE_REWARD_SUCCESS:
      return {
        ...state,
        userRewards: action.payload,
        isFetchingUserReward: false,
      }
    case FETCH_SINGLE_REWARD_FAILED:
      return {
        ...state,
        userRewards: null,
        isFetchingUserReward: false,
      }
    case BUY_REWARD:
      return {
        ...state,
        isAddingReward: true,
      }
    case BUY_REWARD_SUCCESS:
      return {
        ...state,
        isAddingReward: false,
      }
    case BUY_REWARD_FAILED:
      return {
        ...state,
        isAddingReward: false,
      }
    default:
      return state;
  }
};

export default profile;
