import { combineReducers } from 'redux';
import notifyReducer from 'react-redux-notify';

import auth from './auth';
import basket from './basket';
import restaurants from './restaurants';
import profile from './profile';

export default combineReducers({
  auth,
  restaurants,
  basket,
  profile,
  notifications: notifyReducer
});
