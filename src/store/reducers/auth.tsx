import { MyAction, AuthState } from '../../types';
import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILED, GET_CURRENT_USER, GET_CURRENT_USER_SUCCESS, GET_CURRENT_USER_FAILED, LOGOUT, REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILED } from '../constants/auth';

const initialState: AuthState = {
  user: null,
  startLogin: false,
  startRegister: false,
};

const auth = (state = initialState, action: MyAction) => {
  switch (action.type) {
    case LOGIN_REQUEST: {
      return {
        ...state,
        startLogin: true,
      }
    }
    case LOGIN_SUCCESS: {
      return {
        ...state,
        startLogin: false,
        user: action.payload
      }
    }
    case LOGIN_FAILED: {
      return {
        ...state,
        startLogin: false,
        user: null,
      }
    }

    case GET_CURRENT_USER: {
      return state
    }
    case GET_CURRENT_USER_SUCCESS: {
      return {
        ...state,
        user: action.payload
      }
    }
    case GET_CURRENT_USER_FAILED: {
      return {
        ...state,
        user: null
      }
    }

    case REGISTER_REQUEST: {
      return {
        ...state,
        startRegister: true,
      }
    }
    case REGISTER_SUCCESS: {
      return {
        ...state,
        startRegister: false,
      }
    }
    case REGISTER_FAILED: {
      return {
        ...state,
        startRegister: false,
      }
    }

    case LOGOUT: {
      return {
        ...state,
        user: null,
      }
    }
    
    default:
      return state;
  }
};

export default auth;
