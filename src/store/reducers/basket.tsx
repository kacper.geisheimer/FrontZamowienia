import { MyAction, BasketState } from '../../types';
import {
  ADD_ITEM_TO_BASKET,
  ADD_ITEM_TO_BASKET_END,
  UPDATE_ITEM_BASKET,
  UPDATE_ITEM_BASKET_END,
  FETCH_PAYMENT_METHODS,
  FETCH_PAYMENT_METHODS_SUCCESS,
  FETCH_PAYMENT_METHODS_FAILED,
  SET_PAYMENT_METHOD,
  CREATE_ORDER,
  CREATE_ORDER_SUCCESS,
  CREATE_ORDER_FAILED
} from '../constants/basket';

const initialState: BasketState = {
  isAddingOrder: false,
  isAddingToBasket: false,
  isUpdateingBasket: false,
  basket: [],
  paymentMethods: [],
  isFetchingPaymentMethods: false,
  selectedPaymentMethod: null,
  creatingOrder: false,
  newOrderId: null,
};

const basket = (state = initialState, action: MyAction) => {
  switch (action.type) {
    case ADD_ITEM_TO_BASKET:
      return {
        ...state,
        isAddingToBasket: true,
      }
    case ADD_ITEM_TO_BASKET_END:
      return {
        ...state,
        isAddingToBasket: false,
        basket: action.payload
      }
    case UPDATE_ITEM_BASKET:
      return {
        ...state,
        isUpdateingBasket: true,
      }
    case UPDATE_ITEM_BASKET_END:
      return {
        ...state,
        isUpdateingBasket: false,
        basket: action.payload,
      }
    case FETCH_PAYMENT_METHODS:
      return {
        ...state,
        isFetchingPaymentMethods: false,
      }
    case FETCH_PAYMENT_METHODS_SUCCESS:
      return {
        ...state,
        isFetchingPaymentMethods: false,
        paymentMethods: action.payload,
      }
    case FETCH_PAYMENT_METHODS_FAILED:
      return {
        ...state,
        isFetchingPaymentMethods: false,
        paymentMethods: [],
      }
    case SET_PAYMENT_METHOD:
      return {
        ...state,
        selectedPaymentMethod: action.payload,
      }
    case CREATE_ORDER:
      return {
        ...state,
        newOrderId: null,
        creatingOrder: false,
      }
    case CREATE_ORDER_SUCCESS:
      return {
        ...state,
        creatingOrder: false,
        basket: [],
        selectedPaymentMethod: null,
        newOrderId: action.payload,
      }
    case CREATE_ORDER_FAILED:
      return {
        ...state,
        newOrderId: null,
        creatingOrder: false,
      }
    default:
      return state;
  }
};

export default basket;
