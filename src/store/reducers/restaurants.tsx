import { MyAction, RestaurantsState } from '../../types';
import {
  FETCH_RESTAURANTS,
  FETCH_RESTAURANTS_SUCCESS,
  FETCH_RESTAURANTS_FAILED,
  FETCH_RESTAURANT,
  FETCH_RESTAURANT_SUCCESS,
  FETCH_RESTAURANT_FAILED,
  FETCH_MEALS_SUCCESS,
  FETCH_MEALS_FAILED,
  FETCH_MEALS,
} from '../constants/restaurants';

const initialState: RestaurantsState = {
  restaurants: [],
  isFetchingRestaurants: false,
  isFetchedRestaurants: false,
  restaurant: null,
  isFetchingRestaurant: false,
  isFetchedRestaurant: false,
  isFetchingMealsRestaurant: false,
  restaurantMeals: [],
};

const auth = (state = initialState, action: MyAction) => {
  switch (action.type) {
    case FETCH_RESTAURANTS:
      return {
        ...state,
        isFetchingRestaurants: true,
        isFetchedRestaurants: false,
      }
    case FETCH_RESTAURANTS_SUCCESS:
      return {
        ...state,
        isFetchingRestaurants: false,
        isFetchedRestaurants: true,
        restaurants: action.payload,
      }
    case FETCH_RESTAURANTS_FAILED:
      return {
        ...state,
        isFetchingRestaurants: false,
        isFetchedRestaurants: true,
        restaurants: [],
      }
    case FETCH_RESTAURANT:
      return {
        ...state,
        isFetchingRestaurant: true,
        isFetchedRestaurant: false,
      }
    case FETCH_RESTAURANT_SUCCESS:
      return {
        ...state,
        isFetchingRestaurant: false,
        isFetchedRestaurant: true,
        restaurant: action.payload,
      }
    case FETCH_RESTAURANT_FAILED:
      return {
        ...state,
        isFetchingRestaurant: false,
        isFetchedRestaurant: true,
        restaurant: null,
      }
    case FETCH_MEALS:
      return {
        ...state,
        isFetchingMealsRestaurant: true,
      }
    case FETCH_MEALS_SUCCESS:
      return {
        ...state,
        isFetchingMealsRestaurant: false,
        restaurantMeals: action.payload,
      }
    case FETCH_MEALS_FAILED:
      return {
        ...state,
        isFetchingMealsRestaurant: false,
        restaurantMeals: [],
      }
    default:
      return state;
  }
};

export default auth;
