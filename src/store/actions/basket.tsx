import { Dispatch } from 'redux';
import axios from 'axios';

import { 
  ADD_ITEM_TO_BASKET, 
  ADD_ITEM_TO_BASKET_END, 
  UPDATE_ITEM_BASKET, 
  UPDATE_ITEM_BASKET_END, 
  FETCH_PAYMENT_METHODS,
  FETCH_PAYMENT_METHODS_SUCCESS,
  FETCH_PAYMENT_METHODS_FAILED,
  SET_PAYMENT_METHOD,
  CREATE_ORDER,
  CREATE_ORDER_SUCCESS,
  CREATE_ORDER_FAILED
} from '../constants/basket';
import { updateModes } from '../../config';
import { PaymentMethodType } from '../../types/basketTypes';
import { showNotification } from './global';
import { Notification } from '../../types';

export const addItemToBasketBegin = () => ({
  type: ADD_ITEM_TO_BASKET,
})

export const addItemToBasketEnd = (basket) => ({
  type: ADD_ITEM_TO_BASKET_END,
  payload: basket
})

export const addItemToBasket = (item: any) => (dispatch: any, getState: any) => {
  dispatch(addItemToBasketBegin())

  const basket = getState().basket.basket;
  const index = basket.findIndex(v => v.id === item.id);

  if (index > -1) {
    basket[index].count += 1;
  } else {
    basket.push(item);
  }

  dispatch(addItemToBasketEnd(basket));
}

export const updateBasketItemBegin = () => ({
  type: UPDATE_ITEM_BASKET,
})

export const updateBasketItemEnd = (basket) => ({
  type: UPDATE_ITEM_BASKET_END,
  payload: basket
})

export const updateBasketItem = (item: any, mode: string = '') => (dispatch: Dispatch, getState: any) => {
  dispatch(updateBasketItemBegin())

  const basket = getState().basket.basket;
  const index = basket.findIndex(v => v.id === item.id);

  if (index > -1) {
    if (mode === updateModes.INCREASE) {
      basket[index].count += 1;
    }

    if (mode === updateModes.DECREASE) {
      basket[index].count -= 1;
    }

    if (basket[index].count <= 0) {
      basket.splice(index, 1);
    }
  }

  dispatch(updateBasketItemEnd(basket))
}

export const fetchPaymentMethodsBegin = () => ({
  type: FETCH_PAYMENT_METHODS
});

export const fetchPaymentMethodsSuccess = (methods: PaymentMethodType) => ({
  type: FETCH_PAYMENT_METHODS_SUCCESS,
  payload: methods
});

export const fetchPaymentMethodsFailed = () => ({
  type: FETCH_PAYMENT_METHODS_FAILED
});

export const fetchPaymentMethods = () => (dispatch: Dispatch) => {
  dispatch(fetchPaymentMethodsBegin());

  return axios.get(`/payments/`)
  .then(r => {
    dispatch(fetchPaymentMethodsSuccess(r.data));
  })
  .catch(() => {
    dispatch(fetchPaymentMethodsFailed());
  })
}

export const createOrderBegin = () => ({
  type: CREATE_ORDER
});

export const createOrderSuccess = (orderId: string) => ({
  type: CREATE_ORDER_SUCCESS,
  payload: orderId
});

export const createOrderFailed = () => ({
  type: CREATE_ORDER_FAILED
});

export const createOrder = () => (dispatch: any, getState: any) => {
  dispatch(createOrderBegin());
  const basket = getState().basket;

  return axios.post(`/order/`, {
    paymentMethod: basket.selectedPaymentMethod,
    meals: basket.basket,
  })
  .then(r => {
    const notification: Notification = {
      message: 'Zamówienie zostało złożone',
      type: 'info'
    }
    dispatch(showNotification(notification))
    dispatch(createOrderSuccess(r.data));
  })
  .catch(() => {
    dispatch(createOrderFailed());
  })
}

export const setPaymentMethod = (id: number) => ({
  type: SET_PAYMENT_METHOD,
  payload: id,
})