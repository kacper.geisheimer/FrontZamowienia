import React from 'react';

import {
    createNotification, 
    NOTIFICATION_TYPE_SUCCESS, 
    NOTIFICATION_TYPE_INFO,
    NOTIFICATION_TYPE_DANGER,
    NOTIFICATION_TYPE_WARNING
} from 'react-redux-notify';
import { Dispatch } from 'redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faRemoveFormat } from '@fortawesome/free-solid-svg-icons';
import { Notification } from '../../types';

const types = {
    success: NOTIFICATION_TYPE_SUCCESS,
    error: NOTIFICATION_TYPE_DANGER,
    info: NOTIFICATION_TYPE_INFO,
    warning: NOTIFICATION_TYPE_WARNING
}

export const showNotification = (data: Notification) => (dispatch: Dispatch) => {
    const { message = '', type = 'info', duration = 5000, canDismiss = true } = data;

    dispatch(createNotification({
        message,
        type: types[type],
        duration,
        canDismiss,
        icon: <FontAwesomeIcon icon={faRemoveFormat} />
    }));
}