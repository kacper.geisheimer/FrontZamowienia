
import { Dispatch } from 'redux';
import axios from 'axios';

import { 
  LOGIN_REQUEST, 
  LOGIN_SUCCESS, 
  LOGIN_FAILED, 
  GET_CURRENT_USER, 
  GET_CURRENT_USER_SUCCESS, 
  LOGOUT, 
  REGISTER_REQUEST, 
  REGISTER_SUCCESS, 
  REGISTER_FAILED 
} from '../constants/auth';
import { User, Notification, Register } from '../../types';
import { showNotification } from './global';

const loginRequest = () => ({
  type: LOGIN_REQUEST
});

const loginSuccess = (user: User) => ({
  type: LOGIN_SUCCESS,
  payload: user,
});

const loginFailed = () => ({
  type: LOGIN_FAILED
});

export const login = (email: string, password: string) => async (dispatch: any) => {
  dispatch(loginRequest());

  return axios.post(`/auth/`, {
    email,
    password
  })
    .then(r => {
      localStorage.setItem('token', r.data.token);
      axios.defaults.headers['Authorization'] = `JWT ${r.data.token}`;

      currentUser()
        .then((response: any) => {
          dispatch(loginSuccess(response));
        })
        .catch(err => {
          dispatch(loginFailed());
        })

    })
    .catch(err => {
      const errorNotification: Notification = {
        message: 'Podano błędne dane logowania',
        type: 'warning'
      }
      dispatch(showNotification(errorNotification))
      dispatch(loginFailed());
    })
}

const currentUser = () => new Promise((resolve, reject) => {
  return axios.get(`/core/me/`)
    .then((response: any) => {
      resolve(response.data);
    })
    .catch(err => {
      reject(err);
    })
})

const getCurrentUserRequest = () => ({
  type: GET_CURRENT_USER
});

const getCurrentUserSuccess = (user: User) => ({
  type: GET_CURRENT_USER_SUCCESS,
  payload: user,
});

const getCurrentUserFailed = () => ({
  type: LOGIN_FAILED
});

export const getCurrentUser = () => (dispatch: Dispatch) => {
  dispatch(getCurrentUserRequest());

  return axios.get(`/core/me/`)
    .then((response: any) => {
      dispatch(getCurrentUserSuccess(response.data));
    })
    .catch(err => {
      dispatch(getCurrentUserFailed());
    })
}

const logoutUser = () => ({
  type: LOGOUT
});

export const logout = () => (dispatch: any) => {

  const logoutNotification: Notification = {
    message: 'Zostałeś wylogowany!',
    type: 'info'
  };

  localStorage.removeItem('token');
  axios.defaults.headers['Authorization'] = ``;

  dispatch(logoutUser())
  dispatch(showNotification(logoutNotification))
}


const registerRequest = () => ({
  type: REGISTER_REQUEST
});

const registerSuccess = () => ({
  type: REGISTER_SUCCESS,
});

const registerFailed = () => ({
  type: REGISTER_FAILED
});

export const register = (data: Register) => async (dispatch: any) => {
  dispatch(registerRequest());

  return axios.post(`/core/register/`, data)
    .then(r => {
      const successNotification: Notification = {
        message: 'Konto zostało założone. Możesz się zalogować.',
        type: 'success'
      }
      dispatch(showNotification(successNotification))
      dispatch(registerSuccess());
    })
    .catch(err => {
      const errorNotification: Notification = {
        message: 'Wystąpił błąd podczas rejestracji.',
        type: 'warning'
      }
      dispatch(showNotification(errorNotification))
      dispatch(registerFailed());
    })
}