import { Dispatch } from 'redux';
import axios from 'axios';

import { OrdersType, SingleOrderType, RewardType, UserRewardType } from '../../types/profileType';
import {
  FETCH_ORDERS,
  FETCH_ORDERS_SUCCESS,
  FETCH_ORDERS_FAILED,
  FETCH_SINGLE_ORDER,
  FETCH_SINGLE_ORDER_SUCCESS,
  FETCH_SINGLE_ORDER_FAILED,
  FETCH_REWARDS,
  FETCH_REWARDS_SUCCESS,
  FETCH_REWARDS_FAILED,
  FETCH_SINGLE_REWARD,
  FETCH_SINGLE_REWARD_SUCCESS,
  FETCH_SINGLE_REWARD_FAILED,
  BUY_REWARD,
  BUY_REWARD_SUCCESS,
  BUY_REWARD_FAILED
} from '../constants/profile';
import { showNotification } from './global';
import { Notification } from '../../types';
import { getCurrentUser } from './auth';

export const fetchOrdersBegin = () => ({
  type: FETCH_ORDERS
});

export const fetchOrdersSuccess = (orders: OrdersType[]) => ({
  type: FETCH_ORDERS_SUCCESS,
  payload: orders
});

export const fetchOrdersFailed = () => ({
  type: FETCH_ORDERS_FAILED
});

export const fetchOrders = () => (dispatch: Dispatch) => {
  dispatch(fetchOrdersBegin());

  return axios.get(`/orders/`)
    .then(r => {
      dispatch(fetchOrdersSuccess(r.data));
    })
    .catch(() => {
      dispatch(fetchOrdersFailed());
    })
}

export const fetchSingleOrderBegin = () => ({
  type: FETCH_SINGLE_ORDER
});

export const fetchSingleOrderSuccess = (singleOrder: SingleOrderType) => ({
  type: FETCH_SINGLE_ORDER_SUCCESS,
  payload: singleOrder
});

export const fetchSingleOrderFailed = () => ({
  type: FETCH_SINGLE_ORDER_FAILED
});

export const fetchSingleOrder = (id: string) => (dispatch: Dispatch) => {
  dispatch(fetchSingleOrderBegin());

  return axios.get(`/order/${id}/`)
    .then(r => {
      dispatch(fetchSingleOrderSuccess(r.data));
    })
    .catch(() => {
      dispatch(fetchSingleOrderFailed());
    })
}

export const fetchRewardsBegin = () => ({
  type: FETCH_REWARDS
});

export const fetchRewardsSuccess = (rewards: RewardType[]) => ({
  type: FETCH_REWARDS_SUCCESS,
  payload: rewards
});

export const fetchRewardsFailed = () => ({
  type: FETCH_REWARDS_FAILED
});

export const fetchRewards = () => (dispatch: Dispatch) => {
  dispatch(fetchRewardsBegin());

  return axios.get(`/core/rewards/`)
    .then(r => {
      dispatch(fetchRewardsSuccess(r.data));
    })
    .catch(() => {
      dispatch(fetchRewardsFailed());
    })
}

export const fetchUserRewardsBegin = () => ({
  type: FETCH_SINGLE_REWARD
});

export const fetchUserRewardsSuccess = (userRewards: UserRewardType[]) => ({
  type: FETCH_SINGLE_REWARD_SUCCESS,
  payload: userRewards
});

export const fetchUserRewardsFailed = () => ({
  type: FETCH_SINGLE_REWARD_FAILED
});

export const fetchUserRewards = () => (dispatch: Dispatch) => {
  dispatch(fetchUserRewardsBegin());

  return axios.get(`/core/rewards/me/`)
    .then(r => {
      dispatch(fetchUserRewardsSuccess(r.data));
    })
    .catch(() => {
      dispatch(fetchUserRewardsFailed());
    })
}

export const buyRewardBegin = () => ({
  type: BUY_REWARD
});

export const buyRewardEnd = () => ({
  type: BUY_REWARD_SUCCESS
});

export const buyRewardFailed = () => ({
  type: BUY_REWARD_FAILED
});

export const buyReward = (id: number) => (dispatch: any) => {
  dispatch(buyRewardBegin());

  return axios.post(`/core/rewards/me/${id}/`)
    .then(r => {
      const notification: Notification = {
        message: r.data,
        type: 'info'
      }
      dispatch(getCurrentUser());
      dispatch(fetchUserRewards());
      dispatch(showNotification(notification))
      dispatch(buyRewardEnd());
    })
    .catch((err) => {
      dispatch(buyRewardFailed());
    })
}