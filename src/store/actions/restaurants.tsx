import { Dispatch } from 'redux';
import axios from 'axios';

import { 
  FETCH_RESTAURANTS, 
  FETCH_RESTAURANTS_SUCCESS, 
  FETCH_RESTAURANTS_FAILED,
  FETCH_RESTAURANT, 
  FETCH_RESTAURANT_SUCCESS, 
  FETCH_RESTAURANT_FAILED,
  FETCH_MEALS,
  FETCH_MEALS_FAILED,
  FETCH_MEALS_SUCCESS,
} from '../constants/restaurants';
import { MealRestaurantType } from '../../types/restaurantsType';

export const fetchRestaurantsRequest = () => ({
  type: FETCH_RESTAURANTS
})

export const fetchRestaurantsSuccess = (restaurants) => ({
  type: FETCH_RESTAURANTS_SUCCESS,
  payload: restaurants,
})

export const fetchRestaurantsFailed = () => ({
  type: FETCH_RESTAURANTS_FAILED
})

export const fetchRestaurants = () => (dispatch: Dispatch) => {
  dispatch(fetchRestaurantsRequest());

  return axios.get(`/restaurants/`)
    .then(r => {
      dispatch(fetchRestaurantsSuccess(r.data));
    })
    .catch(err => {
      dispatch(fetchRestaurantsFailed());
    })
}

export const fetchRestaurantRequest = () => ({
  type: FETCH_RESTAURANT
})

export const fetchRestaurantSuccess = (restaurants) => ({
  type: FETCH_RESTAURANT_SUCCESS,
  payload: restaurants,
})

export const fetchRestaurantFailed = () => ({
  type: FETCH_RESTAURANT_FAILED
})

export const fetchRestaurant = (id: number) => (dispatch: Dispatch) => {
  dispatch(fetchRestaurantRequest());

  return axios.get(`/restaurants/${id}/`)
    .then(r => {
      dispatch(fetchRestaurantSuccess(r.data));
    })
    .catch(err => {
      dispatch(fetchRestaurantFailed());
    })
}

export const fetchMealsRestaurantRequest = () => ({
  type: FETCH_MEALS
})

export const fetchMealsRestaurantSuccess = (meals: MealRestaurantType[]) => ({
  type: FETCH_MEALS_SUCCESS,
  payload: meals,
})

export const fetchMealsRestaurantFailed = () => ({
  type: FETCH_MEALS_FAILED
})

export const fetchMealsRestaurant = (id: number) => (dispatch: Dispatch) => {
  dispatch(fetchMealsRestaurantRequest());

  return axios.get(`/meals/restaurant/${id}/`)
    .then(r => {
      dispatch(fetchMealsRestaurantSuccess(r.data));
    })
    .catch(err => {
      dispatch(fetchMealsRestaurantFailed());
    })
}