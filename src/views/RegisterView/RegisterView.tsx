import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { History, Location } from 'history';

import { register } from '../../store/actions/auth';
import { RegisterForm } from '../../forms'

import { RootState, Register, User } from '../../types';
import { routes } from '../../config';

export interface RegisterViewProps {
  register(data: Register): void;
  startRegister: boolean;
  user: User,
  history: History,
  location: Location,
}

class RegisterView extends Component<RegisterViewProps, any> {
  componentDidMount() {
    if(this.props.user) {
      this.props.history.push(routes.homeView);
    }
  }
  componentDidUpdate() {
    if(this.props.user) {
      this.props.history.push(routes.homeView);
    }
  }

  render() {
    return (
      <div>
        <h1>Rejestracja</h1>
        <RegisterForm 
          register={this.props.register}
          startRegister={this.props.startRegister}
        />
      </div>);
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    startRegister: state.auth.startRegister,
    user: state.auth.user,
  }
}

const mapDispatchToProps = {
  register
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RegisterView)); 
