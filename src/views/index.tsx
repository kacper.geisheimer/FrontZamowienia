export { default as HomeView } from './HomeView/HomeView';
export { default as NotFound } from './NotFound/NotFound';
export { default as LoginView } from './LoginView/LoginView';
export { default as RegisterView } from './RegisterView/RegisterView';
export { default as ProfileView } from './ProfileView';
export { default as RestaurantView } from './RestaurantView';
export { default as BasketView } from './BasketView';
export { default as OrderView } from './OrderView';
