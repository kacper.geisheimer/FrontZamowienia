import React, { Component } from 'react'
import { Row, Col, Nav, Tab } from 'react-bootstrap';

import './ProfileView.scss';
import Orders from '../../components/Orders';
import { RootState } from '../../types';
import { fetchOrders } from '../../store/actions/profile';
import { connect } from 'react-redux';
import ProfileInfo from '../../components/ProfileInfo';
import RewardsList from '../../components/RewardsList';
import ProfileRewards from '../../components/ProfileRewards';

export interface ProfileViewProps {
  fetchOrders: () => void;
}

export interface ProfileViewState {}

class ProfileView extends Component<ProfileViewProps, ProfileViewState> {
  render() {
    return (<div className="profile-view">
      <h1>Profil użytkownika</h1>
      <Tab.Container id="left-tabs-example" defaultActiveKey="0">
        <Row>
          <Col lg={4}>
            <Nav variant="pills" className="flex-column" onClick={(e) => this.setState({ activeTab: e.target.value })}>
              <h2 className="profile-view__menu-title">Menu</h2>
              <Nav.Item>
                <Nav.Link eventKey="0">Profil użytkownika</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="1" onClick={() => this.props.fetchOrders()}>Zamówienia</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link eventKey="2">Program lojalnościowy</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col lg={8}>
            <Tab.Content>
              <Tab.Pane eventKey="0">
                <ProfileInfo />
                <ProfileRewards />
              </Tab.Pane>
              <Tab.Pane eventKey="1">
                <Orders />
              </Tab.Pane>
              <Tab.Pane eventKey="2">
                <RewardsList />
              </Tab.Pane>
            </Tab.Content>
          </Col>
        </Row>
      </Tab.Container>
    </div>
    );
  }
}

const mapStateToProps = (state: RootState) => {
  return {}
}

const mapDispatchToProps = {
  fetchOrders
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
