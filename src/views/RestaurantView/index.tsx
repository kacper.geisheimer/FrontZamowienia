import React, { Component, lazy, Suspense } from 'react'
import { connect } from 'react-redux'
import { withRouter } from "react-router";

import { fetchRestaurant } from '../../store/actions/restaurants';
import { RootState } from '../../types';
import { RestaurantType } from '../../types/restaurantsType';
import { Spinner } from 'react-bootstrap';

const MealRestaurantList = lazy(() => import('../../components/MealRestaurantList'));

interface RestaurantViewProps {
  match: any,
  restaurant: RestaurantType;
  isFetchingRestaurant: boolean;

  fetchRestaurant: (id: number) => void;
}
interface RestaurantViewState {
  restaurant: RestaurantType;
}

class RestaurantView extends Component<RestaurantViewProps, RestaurantViewState> {
  state = {
    restaurant: this.props.restaurant
  }

  componentDidMount() {
    const { id } = this.props.match.params;

    this.props.fetchRestaurant(id);
  }

  componentDidUpdate(prevProps) {
    
    if(JSON.stringify(prevProps.restaurant) !== JSON.stringify(this.props.restaurant)) {
      this.setState({
        restaurant: this.props.restaurant
      })
    }
  }

  renderAddress = (restaurant: RestaurantType) => {
    const number_local = restaurant.number_local ? `/${restaurant.number_local}` : '';

    if (restaurant.street) {
      return `${restaurant.street} ${restaurant.number}${number_local}, ${restaurant.zip_code} ${restaurant.city}`;
    } else {
      return `${restaurant.city} ${restaurant.number}${number_local}, ${restaurant.zip_code}`;
    }
  }

  render() {
    const { restaurant } = this.state;
    const { isFetchingRestaurant } = this.props;

    if (isFetchingRestaurant) return <div className="list-restaurants"><Spinner animation="border" /></div>
    if(!restaurant || restaurant.detail) return <div style={{ textAlign: 'center' }}>Nie ma takiej restauracji</div>

    return (
      <div>
        <h1>{restaurant && restaurant.name}</h1>
        <p><b>Adres:</b> {restaurant && this.renderAddress(restaurant)}</p>
        <Suspense fallback={<div>Loading...</div>}>
          <MealRestaurantList />
        </Suspense>
      </div>);
  }
}

const mapStateToProps = (state: RootState, ownProps: any) => ({
  restaurant: state.restaurants.restaurant,
  isFetchingRestaurant: state.restaurants.isFetchingRestaurant
})

const mapDispatchToProps = {
  fetchRestaurant
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(RestaurantView));