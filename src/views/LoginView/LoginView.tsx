import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { connect } from 'react-redux'
import { History, Location } from 'history';

import { LoginForm } from '../../forms'
import { login } from '../../store/actions/auth';

import { RootState, User } from '../../types';
import { ROUTING_AFTER_LOGIN } from '../../config';

export interface LoginViewProps {
  login(email: string, password: string): void;
  startLogin: boolean;
  user: User;
  location: Location;
  history: History;
}

class LoginView extends Component<LoginViewProps, any> {

  componentDidMount() {
    if(this.props.user) {
      this.props.history.push(ROUTING_AFTER_LOGIN);
    }
  }
  componentDidUpdate() {
    if(this.props.user) {
      const redirect = this.props.location.search.split('=');
      this.props.history.push(redirect[1] ? `/${redirect[1]}` : ROUTING_AFTER_LOGIN);
    }
  }

  render() {
    const { login, startLogin } = this.props

    return (
      <div>
        <h1>Logowanie</h1>
        <LoginForm
          login={login}
          startLogin={startLogin}
        />
      </div>);
  }
}

const mapStateToProps = (state: RootState) => {
  return {
    startLogin: state.auth.startLogin,
    user: state.auth.user,
  }
}

const mapDispatchToProps = {
  login
}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(LoginView)); 
