import React, { useEffect } from 'react'
import { Row, Col, Spinner } from 'react-bootstrap'

import OrderInfo from '../../components/OrderInfo'
import OrderMealsList from '../../components/OrderMealsList'

import { OrderViewContext } from './context';
import { useDispatch, useSelector } from 'react-redux';
import { fetchSingleOrder } from '../../store/actions/profile';
import { useParams } from 'react-router-dom';
import { RootState } from '../../types';

export interface ParamsType {
  id?: string;
}

const OrderView = () => {
  const dispatch = useDispatch();
  const params: ParamsType = useParams();

  const singleOrder = useSelector((state: RootState) => state.profile.singleOrder);
  const isFetchingSingleOrder = useSelector((state: RootState) => state.profile.isFetchingSingleOrder);

  useEffect(() => {
    fetchOrder();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    fetchOrder();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [params.id]);

  const fetchOrder = () => {
    dispatch(fetchSingleOrder(params.id));
  }


  if (isFetchingSingleOrder) return <div className="list-restaurants"><Spinner animation="border" /></div>

  return (
    <OrderViewContext.Provider value={singleOrder}>
      <Row>
        <Col lg={12}>
          <h1>Informacje o zamówieniu</h1>
          <OrderInfo />
          <OrderMealsList />
        </Col>
      </Row>
    </OrderViewContext.Provider>);
}

export default OrderView;