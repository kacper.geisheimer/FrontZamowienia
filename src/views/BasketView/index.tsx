import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { connect } from 'react-redux'

import { RootState } from '../../types';

import { Row, Col } from 'react-bootstrap';
import BasketList from '../../components/BasketList';
import PaymentMethods from '../../components/PaymentMethods/PaymentMethods';

import './BasketView.scss';
import FinalizeOrder from '../../components/FinalizeOrder';

export interface BasketViewProps { }

class BasketView extends Component<BasketViewProps, any> {
  render() {
    return (
      <>
        <h1>Koszyk</h1>
        <Row>
          <Col lg={12}>
            <BasketList />
          </Col>
        </Row>
        <Row>
          <Col lg={12}>
            <PaymentMethods />
          </Col>
        </Row>
        <Row>
          <Col lg={12} className="basket__finalize">
            <FinalizeOrder />
          </Col>
        </Row>
      </>);
  }
}

const mapStateToProps = (state: RootState) => {
  return {}
}

const mapDispatchToProps = {}


export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BasketView)); 
