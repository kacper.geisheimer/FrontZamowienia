import React, { Component, lazy, Suspense } from 'react'

const ListRestaurants = lazy(() => import('../../components/ListRestaurants'));

class HomeView extends Component<any, any> {
  render() {
    return (
      <div>
        <h1>Restauracje</h1>
        <Suspense fallback={<div>Loading...</div>}>
          <ListRestaurants />
        </Suspense>
      </div>);
  }
}

export default HomeView;