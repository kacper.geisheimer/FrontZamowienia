import { Routes } from '../types';

export const routes:Routes = {
    homeView: '/',
    loginView: '/login',
    registerView: '/register',
    restaurantView: '/restaurant/:id',
    profileView: '/profile',
    basketView: '/basket',
    orderView: '/order/:id'
}

export const ROUTING_AFTER_LOGIN:string = routes.homeView;
export const ROUTING_AFTER_LOGOUT:string = routes.homeView;
