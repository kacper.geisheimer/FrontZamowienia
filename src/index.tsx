import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import { Provider } from 'react-redux';
import { Notify } from 'react-redux-notify';

import configureStore from './store/store';
import App from './App';
import * as serviceWorker from './serviceWorker';

import { handleUnathorized, getToken } from './utils/auth';

import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-redux-notify/dist/ReactReduxNotify.css';
import './styles/main.scss';

const store = configureStore();

axios.defaults.baseURL = process.env.API_URL;
if(getToken()) {
  axios.defaults.headers['Authorization'] = `JWT ${getToken()}`;
}

handleUnathorized(store);

ReactDOM.render(<Provider store={store}>
  <Notify />
  <App />
</Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
