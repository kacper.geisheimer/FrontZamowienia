import React from 'react';
import { connect } from 'react-redux'
import { Route, BrowserRouter as Router } from 'react-router-dom';
import { Container } from 'react-bootstrap';

import { getCurrentUser } from './store/actions/auth';
import { showNotification } from './store/actions/global';

import { routes } from './config';
import { 
  HomeView, 
  LoginView,
  RegisterView,
  ProfileView,
  RestaurantView,
  BasketView,
  OrderView,
} from './views';
import { Header } from './components';
import { RootState, User, Notification } from './types';
import { getToken } from './utils/auth';

export interface AppProps {
  user: User;
  isAddingToBasket: boolean;
  getCurrentUser(): void;
  showNotification(notification: Notification): void;
}

export interface AppState {}

class App extends React.Component<AppProps, AppState> {
  componentDidMount() {
    if(!this.props.user && getToken()) {
      this.props.getCurrentUser();
    }
  }

  render() {
    return (
      <div>
        <Router>
          <Header />
          <Container style={{ marginTop: 15 }}>
            <Route exact path={routes.homeView} component={HomeView} />
            <Route exact path={routes.loginView} component={LoginView} />
            <Route exact path={routes.registerView} component={RegisterView} />
            <Route exact path={routes.profileView} component={ProfileView} />
            <Route exact path={routes.restaurantView} component={RestaurantView} />
            <Route exact path={routes.basketView} component={BasketView} />
            <Route exact path={routes.orderView} component={OrderView} />
          </Container>
        </Router>
      </div>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  user: state.auth.user,
  isAddingToBasket: state.basket.isAddingToBasket,
})

const mapDispatchToProps = {
  getCurrentUser,
  showNotification,
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
