import React from 'react'
import { useSelector } from 'react-redux';

import { RootState } from '../../types';
import { Row, Col } from 'react-bootstrap';
import { renderData } from '../../utils/data';

const ProfileInfo = () => {
  const user = useSelector((state: RootState) => state.auth.user);

  return (
    <Row>
      <Col lg={12}>
        <h1>Informacje o użytkowniku</h1>
        <p>Adres email: {renderData(user, 'email')}</p>
        <p>Punkty lojalnościowe: {renderData(user, 'points')}</p>
      </Col>
    </Row>
  )
}

export default ProfileInfo;
