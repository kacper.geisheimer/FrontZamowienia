import React, { useContext } from 'react'

import { Row, Col, Table } from 'react-bootstrap';
import { OrderViewContext } from '../../views/OrderView/context';

const OrderMealsList = () => {
  const context: any = useContext(OrderViewContext);
  let cost = 0;

  const renderData = () => {
    if (!context || !context.meals.length) return <tr><td colSpan={4} className="basket-no-results">Brak produktów</td></tr>;

    return context.meals.map((meal, i) => {
      cost += meal.count * meal.price;
      
      return <tr>
        <td>{i + 1}</td>
        <td>{meal.name}</td>
        <td>{meal.count}</td>
        <td>{meal.count * meal.price} zł</td>
      </tr>
    })
  }

  return (
    <Row>
      <Col lg={12}>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nazwa produktu</th>
              <th>Ilość</th>
              <th>Koszt</th>
            </tr>
          </thead>
          <tbody>
            {renderData()}
            <tr>
              <td colSpan={4}>
                Całkowity koszt: {cost.toFixed(2)} zł
              </td>
            </tr>
          </tbody>
        </Table>
      </Col>
    </Row>
  )
}

export default OrderMealsList;
