import React, { useState } from 'react'
import { connect } from 'react-redux'
import { History } from 'history';
import {
  Navbar,
  Nav,
} from 'react-bootstrap';
import { Link, withRouter } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faSignOutAlt, faSignInAlt, faUserPlus, faShoppingCart } from '@fortawesome/free-solid-svg-icons'

import { RootState, User } from '../../types';
import { ROUTING_AFTER_LOGOUT, routes } from '../../config';
import { logout } from '../../store/actions/auth';

import './Header.scss';

const { Brand, Toggle, Collapse } = Navbar
const { Item } = Nav;

export interface HeaderProps {
  user: User,
  basket: [],
  logout(): void;
  history: History;
};

const Header = (props: HeaderProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const toggleMenu = () => setIsOpen(!isOpen);

  const logoutUser = () => {
    props.logout();
    props.history.push(ROUTING_AFTER_LOGOUT);
  }

  const loggedUserMenu = () => {
    return <Nav className="ml-auto" navbar>
      <Item>
        <Link className="header__link" to={routes.basketView}>
        <FontAwesomeIcon icon={faShoppingCart} /> Koszyk ({props.basket.length})
        </Link>
      </Item>
      <Item>
        <Link className="header__link" to={routes.profileView}>
          <FontAwesomeIcon icon={faUser} /> Profil
      </Link>
      </Item>
      <Item className="header__link" onClick={logoutUser}>
        <FontAwesomeIcon icon={faSignOutAlt} /> Wyloguj się
      </Item>
    </Nav>
  }

  const notLoggedUserMenu = () => {
    return <Nav className="ml-auto" navbar>
      <Item>
        <Link className="header__link" to="/register">
          <FontAwesomeIcon icon={faUserPlus} /> Zarejestruj się
        </Link>
        <Link className="header__link" to="/login">
          <FontAwesomeIcon icon={faSignInAlt} /> Zaloguj się
        </Link>
      </Item>
    </Nav>
  }

  return (
    <Navbar bg="light" expand="md" className="header">
      <Brand>
        <Link className="header__link" to="/">
          Zamówienia
        </Link>
      </Brand>
      <Toggle onClick={toggleMenu} />
      <Collapse>
        {props.user ? loggedUserMenu() : notLoggedUserMenu()}
      </Collapse>
    </Navbar>);
}

const mapStateToProps = (state: RootState) => ({
  user: state.auth.user,
  basket: state.basket.basket,
})

const mapDispatchToProps = {
  logout
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Header));