import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux';

import Button from '../Button/Button';

import { createOrder } from '../../store/actions/basket';

import { RootState } from '../../types';

const Order = () => {
  const dispatch = useDispatch();

  const basket = useSelector((state: RootState) => state.basket.basket);
  const selectedPaymentMethod = useSelector((state: RootState) => state.basket.selectedPaymentMethod);

  const create = () => {
    dispatch(createOrder());
  }
  return (
    <Button
      disabled={!basket.length || !selectedPaymentMethod}
      onClick={create}
    >
      Zamów
    </Button>
  )
}

export default Order;
