import React, { useEffect } from 'react'
import { useSelector, useDispatch } from 'react-redux';

import { RootState } from '../../types';
import { Row, Col, Table} from 'react-bootstrap';
import { fetchUserRewards } from '../../store/actions/profile';

import './ProfileRewards.scss';
import { UserRewardType } from '../../types/profileType';

const ProfileRewards = () => {
  const userRewards = useSelector((state: RootState) => state.profile.userRewards);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchUserRewards());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const renderRewards = () => {
    if (!userRewards || !userRewards.length) return <tr><td colSpan={4} className="basket-no-results">Brak nagród</td></tr>;

    return userRewards.map((reward: UserRewardType, i: number) => (
      <tr key={i}>
        <td>{i + 1}</td>
        <td>{reward.reward}</td>
        <td>{reward.status}</td>
      </tr>
    ))
  }
  return (
    <Row className="profile-rewards">
      <Col lg={12}>
        <h1>Odebrane nagrody</h1>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Nazwa nagrody</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            {renderRewards()}
          </tbody>
        </Table>
      </Col>
    </Row>
  )
}

export default ProfileRewards;
