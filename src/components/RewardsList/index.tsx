import React, { useEffect } from 'react'
import { Row, Col, Spinner, Card } from 'react-bootstrap'

import { useDispatch, useSelector } from 'react-redux';
import { fetchRewards, buyReward } from '../../store/actions/profile';
import { RootState } from '../../types';
import Button from '../Button/Button';

import './RewardsList.scss';

export interface ParamsType {
  id?: string;
}

const RewardsList = () => {
  const dispatch = useDispatch();

  const rewards = useSelector((state: RootState) => state.profile.rewards);
  const user = useSelector((state: RootState) => state.auth.user);
  const isFetchingRewards = useSelector((state: RootState) => state.profile.isFetchingRewards);
  const isAddingReward = useSelector((state: RootState) => state.profile.isAddingReward);

  useEffect(() => {
    dispatch(fetchRewards());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  if (isFetchingRewards) return <div className="list-restaurants"><Spinner animation="border" /></div>

  console.log(user)
  const renderRewards = () => {
    return rewards.map(reward => (
      <Col lg={6} key={reward.id}>
        <Card className="list-restaurants__card" key={reward.id}>
          <Card.Body>
            <Card.Title className="list-restaurants__title">
              {reward.name}
            </Card.Title>
            <Card.Text className="rewards__content">
              <span className="rewards__cost">Koszt: {reward.cost} punktów</span>
              <Button
                disabled={user && (Number(user.points) < reward.cost)}
                loading={isAddingReward}
                onClick={() => dispatch(buyReward(reward.id))}
              >
                Odbierz
              </Button>
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    ))
  }

  return (
    <>
      <Row>
        <Col lg={12}>
          <h1>Program lojalnościowy</h1>
          <div>
            <p>Za punkty w programie lojalnościowym możesz wybrać nagrodę jaką chcesz otrzymać.</p>
            <p>Twoja liczba punktów wynosi {user && user.points}</p>
          </div>
        </Col>
      </Row>
      <Row>
        {renderRewards()}
      </Row>
    </>
  );
}

export default RewardsList;