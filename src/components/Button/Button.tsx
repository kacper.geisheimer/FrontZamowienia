import React from 'react'
import { Button as ButtonStrap } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';

export interface ButtonProps {
  active?: boolean;
  'aria-label'?: string;
  block?: boolean;
  color?: string;
  loading?: boolean;
  outline?: boolean;
  size?: 'sm' | 'lg';
  children?: any;
  className?: string;
  cssModule?: any;
  close?: boolean;
  tag?: any;
  innerRef?: any;
  disabled?: boolean;
  onClick?(e?: any): any;
}

const Button = (props: ButtonProps) => {
  return <ButtonStrap
    {...props}
    disabled={props.loading || props.disabled}
  >
    {props.children} {props.loading ? <FontAwesomeIcon icon={faSpinner}/> : null}
  </ButtonStrap>
}
    
export default Button;