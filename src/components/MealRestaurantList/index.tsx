import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { connect } from 'react-redux'
import { Spinner, Card, Row, Col, Button } from 'react-bootstrap';
import { History } from 'history';

import { fetchMealsRestaurant } from '../../store/actions/restaurants';
import { addItemToBasket } from '../../store/actions/basket';

import { RootState, User } from '../../types';
import { MealRestaurantType } from '../../types/restaurantsType';

import './MealRestaurantList.scss';
import { routes } from '../../config';

interface MealRestaurantListProps {
  restaurantMeals: MealRestaurantType[];
  isFetchingMealsRestaurant: boolean;
  history: History;
  match: any;
  user: User;

  fetchMealsRestaurant: (id: number) => void;
  addItemToBasket: (item: any) => void;
};
interface MealRestaurantListState { }

class MealRestaurantList extends Component<MealRestaurantListProps, MealRestaurantListState> {
  componentDidMount() {
    this.props.fetchMealsRestaurant(this.props.match.params.id);
  }

  noResults = () => {
    return <>
      <h1 style={{ marginTop: 15 }}>Dania</h1>
      <div className="list-restaurants">W systemie nie ma żadnych dań dla restauracji</div>
    </>
  }

  addItem = (meal: MealRestaurantType) => {
    this.props.addItemToBasket({
      ...meal,
      count: 1,
    });
  }

  redirectToLoginPage = () => {
    this.props.history.push(routes.loginView);
  }

  render() {
    const { restaurantMeals, isFetchingMealsRestaurant, user } = this.props;

    if (isFetchingMealsRestaurant) return <div className="list-restaurants"><Spinner animation="border" /></div>
    if (!restaurantMeals.length) return this.noResults();

    const btnLogged = (meal) => <Button onClick={() => this.addItem(meal)}>Dodaj do koszyka</Button>;
    const btnNotLogged = <Button onClick={() => this.redirectToLoginPage()}>Zaloguj się</Button>
    return (
      <>
        <h1 style={{ marginTop: 15 }}>Dania</h1>
        <Row>
          {restaurantMeals.map(meal => {
            return <Col lg={4} key={meal.id} style={{ marginBottom: 15 }}>
              <Card>
                <Card.Body>
                  <Card.Title>{meal.name}</Card.Title>
                  <Card.Text style={{ paddingBottom: 15 }}>
                    {meal.price.toFixed(2)} zł
                  </Card.Text>
                  {user ? btnLogged(meal) : btnNotLogged}
                </Card.Body>
              </Card>
            </Col>
          })}
        </Row>
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  user: state.auth.user,
  restaurantMeals: state.restaurants.restaurantMeals,
  isFetchingMealsRestaurant: state.restaurants.isFetchingMealsRestaurant,
})

const mapDispatchToProps = {
  fetchMealsRestaurant,
  addItemToBasket,
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(MealRestaurantList));