import React, { useContext } from 'react'
import { Row, Col } from 'react-bootstrap';
import { OrderViewContext } from '../../views/OrderView/context';

import './OrderInfo.scss';

const OrderInfo = () => {
  const context = useContext(OrderViewContext);

  const renderData = (key: string) => {
    if(!context) return '-'

    return context[key];
  }

  const renderStatus = () => {
    if(renderData('status_payment') === '-') return '-';

    return renderData('status_payment') === 'UNPAYED' ? 'Nieopłacone' : 'Opłacone';
  } 

  return (
    <Row className="order-info">
      <Col lg={4}>
        Status zamówienia: {renderData('status_order')}
      </Col>
      <Col lg={4}>
        Metoda płatności: {renderData('payment_method')}
      </Col>
      <Col lg={4}>
        Status płatności: {renderStatus()}
      </Col>
    </Row>
  )
}

export default OrderInfo;
