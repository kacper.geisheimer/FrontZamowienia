import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';

import { RootState } from '../../types';
import { fetchOrders } from '../../store/actions/profile';

import { Table } from 'react-bootstrap';

import './Orders.scss';
import { withRouter } from 'react-router';
import { routes } from '../../config';

const Orders = (props: any) => {
  const dispatch = useDispatch();

  const orders = useSelector((state: RootState) => state.profile.orders);

  useEffect(() => {
    if (!orders.length)
      dispatch(fetchOrders());
  });

  const redirectToOrder = (id: string) => {
    props.history.push(routes.orderView.replace(':id', id));
  }
  
  return (
    <div>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>#</th>
            <th>Numer zamówienia</th>
            <th>Status zamówienia</th>
            <th>Data zamówienia</th>
            <th>Opcje</th>
          </tr>
        </thead>
        <tbody>
          {orders.map((order, i) => {
            return <tr key={order.id}>
              <td>{i + 1}</td>
              <td>{order.id}</td>
              <td>{order.status}</td>
              <td>{moment(order.created).format('DD-MM-YYYY HH:mm')}</td>
              <td className="orders__link" onClick={() => redirectToOrder(order.id)}>Szczegóły</td>
            </tr>
          })}
        </tbody>
      </Table>
    </div >
  )
}

export default withRouter(Orders);
