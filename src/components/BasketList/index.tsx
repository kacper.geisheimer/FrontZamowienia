import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { connect } from 'react-redux'
import { Table } from 'react-bootstrap';

import { RootState } from '../../types';
import { BasketType } from '../../types/basketTypes';
import { updateModes, routes } from '../../config';

import { updateBasketItem } from '../../store/actions/basket';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMinus, faPlus } from '@fortawesome/free-solid-svg-icons'

import './BasketList.scss';
import { History } from 'history';

interface BasketListProps {
  basket: [];
  updateBasketItem: (item: BasketType, mode: string) => void;
  isUpdateingBasket: boolean;
  selectedPaymentMethod: number;
  newOrderId: string;
  history: History;
};
interface BasketListState {
  basket: BasketType[];
};

class BasketList extends Component<BasketListProps, BasketListState> {
  constructor(props) {
    super(props);

    this.state = {
      basket: this.props.basket,
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.isUpdateingBasket !== prevProps.isUpdateingBasket) {
      this.setState({
        basket: this.props.basket,
      })
    }

    if (this.props.selectedPaymentMethod !== prevProps.selectedPaymentMethod) {
      this.setState({
        basket: this.props.basket,
      })
      
      if(this.props.newOrderId !== prevProps.newOrderId) {
        this.props.history.push(routes.orderView.replace(':id', this.props.newOrderId));
      }
    }

  }

  updateItem = (item: BasketType, mode: string) => {
    this.props.updateBasketItem(item, mode);
  }

  render() {
    const { basket } = this.state;
    let cost: number = 0;

    return (
      <>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>lp</th>
              <th>Nazwa produktu</th>
              <th>Ilość</th>
              <th>Cena</th>
            </tr>
          </thead>
          <tbody>
            {!basket.length ? <tr>
              <td colSpan={4} className="basket-no-results">Brak produktów w koszyku</td>
            </tr> :
              <>
                {basket.map((item: BasketType, i: number) => {
                  cost += item.price * item.count;

                  return <tr key={item.id}>
                    <td>{i + 1}</td>
                    <td>{item.name}</td>
                    <td>
                      <span className="basket-icon" onClick={() => this.updateItem(item, updateModes.DECREASE)}>
                        <FontAwesomeIcon icon={faMinus} />
                      </span>
                      {item.count}
                      <span className="basket-icon" onClick={() => this.updateItem(item, updateModes.INCREASE)}>
                        <FontAwesomeIcon icon={faPlus} />
                      </span>
                    </td>
                    <td>{(item.count * item.price).toFixed(2)} zł</td>
                  </tr>
                })}
                <tr>
                  <td colSpan={4}>
                    Całkowity koszt: {cost.toFixed(2)} zł
                  </td>
                </tr>
              </>}

          </tbody>
        </Table>
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  basket: state.basket.basket,
  isUpdateingBasket: state.basket.isUpdateingBasket,
  selectedPaymentMethod: state.basket.selectedPaymentMethod,
  newOrderId: state.basket.newOrderId,
})

const mapDispatchToProps = {
  updateBasketItem
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(BasketList));