import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { fetchPaymentMethods, setPaymentMethod } from '../../store/actions/basket';

import { RootState } from '../../types';
import { PaymentMethodType } from '../../types/basketTypes';

import './PaymentMethods.scss';
import { Spinner, Form } from 'react-bootstrap';

const PaymentMethods = () => {
  const dispatch = useDispatch();
  const paymentMethods: PaymentMethodType[] = useSelector((state: RootState) => state.basket.paymentMethods)
  const isFetchingPaymentMethods: boolean = useSelector((state: RootState) => state.basket.isFetchingPaymentMethods)

  // eslint-disable-next-line
  useEffect(() => {
    dispatch(fetchPaymentMethods());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const selectMethod = (e: any) => {
    const id: number = e.target.value;
    dispatch(setPaymentMethod(id))
  }

  if (isFetchingPaymentMethods) return <div className="list-restaurants"><Spinner animation="border" /></div>

  return (<>
    <h2>Sposób płatności</h2>
    <Form className="payment-method-form" onChange={selectMethod}>
      {paymentMethods.map(method => {
        return <Form.Check key={method.id} type={'radio'} value={method.id} label={method.name} name="method" />
      })}
    </Form>
  </>)
}

// const mapStateToProps = (state: RootState) => ({
//   user: state.auth.user,
//   basket: state.basket.basket,
//   isFetchingPaymentMethods: state.basket.isFetchingPaymentMethods
// })

export default PaymentMethods;