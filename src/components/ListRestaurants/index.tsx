import React, { Component } from 'react'
import { withRouter } from 'react-router';
import { connect } from 'react-redux'
import { Spinner, Card } from 'react-bootstrap';
import { History } from 'history';

import Button from '../Button/Button';

import { fetchRestaurants } from '../../store/actions/restaurants';

import { RootState } from '../../types';
import { RestaurantType } from '../../types/restaurantsType';

import './ListRestaurants.scss';
import { routes } from '../../config';

interface ListRestaurantsProps {
  restaurants: [];
  isFetchingRestaurants: boolean;
  isFetchedRestaurants: boolean;
  fetchRestaurants: () => void;
  history: History;
};
interface ListRestaurantsState { }

class ListRestaurants extends Component<ListRestaurantsProps, ListRestaurantsState> {
  componentDidMount() {
    if (!this.props.restaurants.length)
      this.props.fetchRestaurants();
  }

  noResults = () => {
    return <div className="list-restaurants">W systemie nie ma żadnych restauracji</div>
  }

  renderAddress = (restaurant: RestaurantType) => {
    const number_local = restaurant.number_local ? `/${restaurant.number_local}` : '';

    if (restaurant.street) {
      return `${restaurant.street} ${restaurant.number}${number_local}, ${restaurant.zip_code} ${restaurant.city}`;
    } else {
      return `${restaurant.city} ${restaurant.number}${number_local}, ${restaurant.zip_code}`;
    }
  }

  redirectToRestaurant = (id: number) => {
    this.props.history.push(routes.restaurantView.replace(':id', id.toString()));
  }

  renderRestaurants = () => {
    return this.props.restaurants.map((v: RestaurantType) => {
      return <Card className="list-restaurants__card" key={v.id}>
        <Card.Body>
          <Card.Title className="list-restaurants__title">
            {v.name}
            <Button 
              onClick={() => this.redirectToRestaurant(v.id)}
            >
              Przejdź
            </Button>
          </Card.Title>
          <Card.Text>
            {this.renderAddress(v)}
          </Card.Text>
        </Card.Body>
      </Card>
    });
  }

  render() {
    const { restaurants, isFetchingRestaurants, isFetchedRestaurants } = this.props;

    if (isFetchingRestaurants) return <div className="list-restaurants"><Spinner animation="border" /></div>
    if (isFetchedRestaurants && !restaurants.length) return this.noResults();

    return (
      <>
        {this.renderRestaurants()}
      </>
    );
  }
}

const mapStateToProps = (state: RootState) => ({
  restaurants: state.restaurants.restaurants,
  isFetchingRestaurants: state.restaurants.isFetchingRestaurants,
  isFetchedRestaurants: state.restaurants.isFetchedRestaurants
})

const mapDispatchToProps = {
  fetchRestaurants
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ListRestaurants));