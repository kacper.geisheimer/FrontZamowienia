import axios from 'axios';
import queryString from 'query-string';

import { routes } from '../config';
import { showNotification } from '../store/actions/global';

export const getToken = () => {
  return localStorage.getItem('token');
}

export const handleUnathorized = (store: any) => {
  axios.interceptors.response.use(null, function (error) {
    if (error.response.status === 401) {
      store.dispatch(showNotification({
        message: 'Zostałeś wylogowany! Za chwilę zostanie przekierowany do strony logowania!',
        duration: 5000,
      }));

      const urlParams = queryString.stringify({
        redirect: window.location.pathname
      });

      window.localStorage.removeItem('token');
      if (window.location.pathname !== routes.loginView) {
        setTimeout(() => {
          window.location.href = `${routes.loginView}?${urlParams}`;
        }, 5000)
        
      }
    }
    return Promise.reject(error);
  });
}