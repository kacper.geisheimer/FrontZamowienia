export const renderData = (object: any, key: string) => {
    if(!object || !object[key]) return '-';

    return object[key];
}